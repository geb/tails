# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-05-18 09:54+0200\n"
"PO-Revision-Date: 2018-07-01 15:48+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>Most files contain metadata which is information characterizing the\n"
"content of the file. For example:</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<ul>\n"
"<li>Cameras record data about when and where a picture was taken and what\n"
"camera was used.</li>\n"
"<li>Office documents automatically add author\n"
"and company information to texts and spreadsheets.</li>\n"
"</ul>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>You can use the <span\n"
"class=\"application\"><a href=\"https://0xacab.org/jvoisin/mat2\">MAT</a></span> to\n"
"clean the metadata from your files before publishing them.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"
